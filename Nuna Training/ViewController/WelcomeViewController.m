//
//  WelcomeViewController.m
//  Nuna Training
//
//  Created by Rifat Firdaus on 6/16/15.
//  Copyright (c) 2015 Rifat Firdaus. All rights reserved.
//

#import "WelcomeViewController.h"
#import "UIEnvi.h"
@interface WelcomeViewController ()
@property (weak, nonatomic) IBOutlet UIButton *btnDone;
-(IBAction)btnDoneClk:(id)sender;
@end

@implementation WelcomeViewController

UIEnvi *uienvi;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    uienvi = [[UIEnvi alloc] init];
    
    [uienvi setBorderBtn:_btnDone];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)btnDoneClk:(id)sender{
    [self performSegueWithIdentifier:@"goHome" sender:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
