//
//  InputPhoneNumberViewController.m
//  Nuna Training
//
//  Created by Rifat Firdaus on 6/16/15.
//  Copyright (c) 2015 Rifat Firdaus. All rights reserved.
//

#import "InputPhoneNumberViewController.h"
#import "UIEnvi.h"

@interface InputPhoneNumberViewController ()
@property (weak, nonatomic) IBOutlet UIButton *btnVerify;

-(IBAction)btnVerifyClk:(id)sender;

@end

@implementation InputPhoneNumberViewController

UIEnvi *uienvi;
UIButton *btnVerify2;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    btnVerify2 = _btnVerify;
    uienvi = [[UIEnvi alloc] init];
    
    [uienvi createAlert:@"Ini Judul" msg:@"Na kalo ini pesan!"];
    [uienvi setBorderBtn:btnVerify2];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)btnVerifyClk:(id)sender{
    [self performSegueWithIdentifier:@"goViewCode" sender:self];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
