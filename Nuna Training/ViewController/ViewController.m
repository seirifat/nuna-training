//
//  ViewController.m
//  Nuna Training
//
//  Created by Rifat Firdaus on 6/16/15.
//  Copyright (c) 2015 Rifat Firdaus. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

-(IBAction)btnSignUpCkl:(id)sender;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)btnSignUpCkl:(id)sender{
    [self performSegueWithIdentifier:@"goInputPhonenumber" sender:self];
}

@end
