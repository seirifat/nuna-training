//
//  UIEnvi.h
//  Nuna Training
//
//  Created by Rifat Firdaus on 6/16/15.
//  Copyright (c) 2015 Rifat Firdaus. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>
@interface UIEnvi : NSObject


@property NSString* title;

//public void createAlert(String title, String message, int status);
-(void)createAlert:(NSString*)title msg:(NSString*)msg;
-(void)setBorderBtn:(UIButton*)btn;
@end
