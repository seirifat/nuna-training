//
//  SplitViewMasterTableViewController.h
//  Nuna Training
//
//  Created by Rifat Firdaus on 6/17/15.
//  Copyright (c) 2015 Rifat Firdaus. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SplitViewDetailViewController;

@interface SplitViewMasterTableViewController : UITableViewController

@property (nonatomic, retain) NSArray *siteNames;
@property (nonatomic, retain) NSArray *siteaddresses;

@property (strong,nonatomic) SplitViewDetailViewController *detailViewController;

@end
