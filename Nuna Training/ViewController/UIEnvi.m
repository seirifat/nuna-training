//
//  UIEnvi.m
//  Nuna Training
//
//  Created by Rifat Firdaus on 6/16/15.
//  Copyright (c) 2015 Rifat Firdaus. All rights reserved.
//

#import "UIEnvi.h"

@implementation UIEnvi

-(void)createAlert:(NSString *)title msg:(NSString*)msg{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)setBorderBtn:(UIButton *)btn{
    [[btn layer] setCornerRadius:8.0f];
    [[btn layer] setBorderWidth:1];
    [[btn layer] setBorderColor:[UIColor whiteColor].CGColor];
}

@end
